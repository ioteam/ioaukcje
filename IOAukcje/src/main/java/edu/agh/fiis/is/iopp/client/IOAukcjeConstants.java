package edu.agh.fiis.is.iopp.client;

import com.google.gwt.i18n.client.Constants;

public interface IOAukcjeConstants extends Constants{
	
	  @DefaultStringValue("IOAuctions")
	  String title();
	  
	  @DefaultStringValue("Welcome")
	  String welcomeMessage();

	  @DefaultStringValue("Please provide your user login and password in the form below:")
	  String loginInstructions();
	  
}
