package edu.agh.fiis.is.iopp.um.util;

import javax.persistence.Embeddable;

import edu.agh.fiis.is.iopp.um.util.enums.AddressType;

//TODO: JPA
@Embeddable
public class Address {
	
	private Long id;
	private AddressType address_type;
	private String postal_code;
	private String city;
	private String street_name;
	private String street_number;
	private String street_suffix;
	private String additional_information;
	
	public AddressType getAddress_type() {
		return address_type;
	}
	public void setAddress_type(AddressType address_type) {
		this.address_type = address_type;
	}
	public String getPostal_code() {
		return postal_code;
	}
	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreet_name() {
		return street_name;
	}
	public void setStreet_name(String street_name) {
		this.street_name = street_name;
	}
	public String getStreet_number() {
		return street_number;
	}
	public void setStreet_number(String street_number) {
		this.street_number = street_number;
	}
	public String getStreet_suffix() {
		return street_suffix;
	}
	public void setStreet_suffix(String street_suffix) {
		this.street_suffix = street_suffix;
	}
	public String getAdditional_information() {
		return additional_information;
	}
	public void setAdditional_information(String additional_information) {
		this.additional_information = additional_information;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

}
