package edu.agh.fiis.is.iopp.um.util.enums;

import javax.persistence.Embeddable;

@Embeddable
public enum AddressType {
	PHISICAL, POBOX
}
