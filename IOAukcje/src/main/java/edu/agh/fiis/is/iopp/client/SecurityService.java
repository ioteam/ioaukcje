package edu.agh.fiis.is.iopp.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import edu.agh.fiis.is.iopp.shared.LoginResult;

@RemoteServiceRelativePath("security")
public interface SecurityService extends RemoteService {
  LoginResult login(String username, String password, boolean rememberMe);
  void logout();
}