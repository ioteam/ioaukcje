package edu.agh.fiis.is.iopp.client;


import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.agh.fiis.is.iopp.shared.LoginResult;

public interface SecurityServiceAsync {
  
  void login(String username, String password, boolean rememberMe, AsyncCallback<LoginResult> callback);

	void logout(AsyncCallback<Void> callback);
}
