package edu.agh.fiis.is.iopp.client;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.ui.RootPanel;

import edu.agh.fiis.is.iopp.client.ui.RegisterWidget;

public class RegisterModule implements EntryPoint {

  private Logger LOG = Logger.getLogger("REGISTER");

  public void onModuleLoad() {
    GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {

      public void onUncaughtException(Throwable caught) {
        LOG.log(Level.SEVERE, "uncaught exception", caught);
      }
    });

    Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {

      public void execute() {
        init();
      }
    });
  }

  private void init() {
    RegisterWidget widget = new RegisterWidget();
    RootPanel.get().add(widget);
  }
}
