package edu.agh.fiis.is.iopp.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import edu.agh.fiis.is.iopp.shared.RegisterResult;

@RemoteServiceRelativePath("register")
public interface RegisterService extends RemoteService {
  RegisterResult registerCompanyParty(String emial, String password, String login, String cp_name, String cp_phone, String cp_address_1, String cp_address_2, String counrty, String website);
}